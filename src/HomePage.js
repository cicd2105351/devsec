import React, { useState } from 'react';
import axios from 'axios';

function HomePage() {
  const [departement, setDepartement] = useState('');
  const [region, setRegion] = useState('');
  const [maxHabitants, setMaxHabitants] = useState('');
  const [maxDensite, setMaxDensite] = useState('');
  const [maxMoins20Ans, setMaxMoins20Ans] = useState('');
  const [results, setResults] = useState([]);
  const [noResults, setNoResults] = useState(false);

  const handleSearch = async () => {
    try {
      const response = await axios.post('http://localhost:8080/search', {
        departement,
        region,
        maxHabitants,
        maxDensite,
        maxMoins20Ans,
      });
      // Trier les résultats par année
      const sortedResults = response.data.sort((a, b) => b.annee_publication - a.annee_publication);
      setResults(sortedResults);
      setNoResults(sortedResults.length === 0);
    } catch (error) {
      console.error('Error during search:', error);
      setNoResults(true);
    }
  };

  return (
    <div style={{ textAlign: 'center', padding: '20px' }}>
      <h1>Statistiques de Logement</h1>
      <div style={{ marginBottom: '10px' }}>
        <label>Département : </label>
        <input
          type="text"
          value={departement}
          onChange={(e) => setDepartement(e.target.value)}
          placeholder="Saisissez le département..."
        />
      </div>
      <div style={{ marginBottom: '10px' }}>
        <label>Région : </label>
        <input
          type="text"
          value={region}
          onChange={(e) => setRegion(e.target.value)}
          placeholder="Saisissez la région..."
        />
      </div>
      <div style={{ marginBottom: '10px' }}>
        <label>Nombre d'habitants inférieur à : </label>
        <input
          type="number"
          value={maxHabitants}
          onChange={(e) => setMaxHabitants(e.target.value)}
          placeholder="Saisissez le nombre d'habitants maximum..."
        />
      </div>
      <div style={{ marginBottom: '10px' }}>
        <label>Densité de population au km² inférieure à : </label>
        <input
          type="number"
          value={maxDensite}
          onChange={(e) => setMaxDensite(e.target.value)}
          placeholder="Saisissez la densité de population maximum..."
        />
      </div>
      <div style={{ marginBottom: '10px' }}>
        <label>Pourcentage de moins de 20 ans dans la population inférieur à:</label>
        <input
          type="number"
          value={maxMoins20Ans}
          onChange={(e) => setMaxMoins20Ans(e.target.value)}
          placeholder="Saisissez le pourcentage maximum..."
        />
      </div>
      <button onClick={handleSearch}>Rechercher</button>
      {noResults && <p>Aucun résultat trouvé.</p>}
      <div>
        <h2>Résultats de la recherche:</h2>
        {results.length === 0 ? (
          <p>Aucun résultat trouvé.</p>
        ) : (
          <table style={{ width: '100%', borderCollapse: 'collapse', marginTop: '10px' }}>
            <thead>
              <tr>
                <th>Année</th>
                <th>Département</th>
                <th>Région</th>
                <th>Habitants</th>
                <th>Densité de population au km²</th>
                <th>Pourcentage moins de 20 ans</th>
              </tr>
            </thead>
            <tbody>
              {results.map((result) => (
                <tr key={`${result.annee_publication}-${result.code_departement}-${result.code_region}`}>
                  <td>{result.annee_publication}</td>
                  <td>{result.nom_departement}</td>
                  <td>{result.nom_region}</td>
                  <td>{result.nombre_d_habitants}</td>
                  <td>{result.densite_de_population_au_km2}</td>
                  <td>{result.population_de_moins_de_20_ans }</td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </div>
    </div>
  );
}

export default HomePage;
