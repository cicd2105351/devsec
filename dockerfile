# Utilisez une image de base qui inclut Node.js
FROM node:18

# Installez Git
RUN apt-get update && apt-get install -y git

# Définissez le répertoire de travail
WORKDIR /app

# Copiez les fichiers package.json et package-lock.json pour installer les dépendances
COPY package*.json ./

# Installez les dépendances
RUN npm install

# Copiez le reste des fichiers de l'application
COPY . .

# Exposez le port sur lequel le serveur sera en écoute
EXPOSE 8080

# Commande pour démarrer l'application
CMD ["npm", "start"]

